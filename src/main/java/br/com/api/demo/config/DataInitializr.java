package br.com.api.demo.config;

import br.com.api.demo.entity.Role;
import br.com.api.demo.entity.User;
import br.com.api.demo.repository.RoleRepository;
import br.com.api.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class DataInitializr implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        List<User> users = userRepository.findAll();
        if (users.isEmpty()) {
            this.createUsers("Dhiego", "dhiego@gmail.com", passwordEncoder.encode("123456"), "ROLE_ADMIN");
            this.createUsers("Joao", "joao@gmail.com", passwordEncoder.encode("123456"), "ROLE_ADMIN");
        }
    }

    private void createUsers(String name, String email, String password, String role) {
        Role roleObject = new Role();
        roleObject.setName(role);

        roleRepository.save(roleObject);

        User user = new User(name, email, password, Arrays.asList(roleObject));
        userRepository.save(user);
    }
}
